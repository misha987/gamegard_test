<?php

require("myD2ApiClass.php");

$arNicknames = file_get_contents("input.log");
$arNicknames = explode(PHP_EOL, $arNicknames);

$myApiKey = "41e2aaccb0644505ab3cc8fe4caf7b7d";

$objD2Api = new D2Api($myApiKey);

$itemId = 3260604717;

$resStr = "";

foreach ($arNicknames as $currentNickname) {
    $findPlayers = $objD2Api->searchPlayer($currentNickname); // Получение всех игроков с текущим никнеймом

    foreach ($findPlayers as $currentPlayer) {
        $resProfile = $objD2Api->getProfile($currentPlayer->membershipId, $currentPlayer->membershipType); // Получение данных пользовтеля по id и типу учетной записи

        $resStr .= $currentNickname;
        if (isset($resProfile->Response->profileCollectibles->data) &&
            property_exists($resProfile->Response->profileCollectibles->data->collectibles, $itemId) &&
            $resProfile->Response->profileCollectibles->data->collectibles->$itemId->state == 0) {
            $resStr .= " - has ";
        } else if (!isset($resProfile->Response->profileCollectibles->data) &&
            isset($resProfile->Response->profileCollectibles->privacy) &&
            $resProfile->Response->profileCollectibles->privacy == 2) {
            $resStr .= " - unknown ";
        }
        else {
            $resStr .= " - no ";
        }
        $resStr .= (count($findPlayers) > 1 ? $objD2Api->getTypeName($currentPlayer->membershipType) : "") . "\n";
    }
}

file_put_contents("output.log", $resStr);

?>