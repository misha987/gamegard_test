<?php

class D2Api
{

    private $apiKey;
    private $mainUrl = "https://www.bungie.net/Platform";

    private $arTypes = Array(
        "Xbox" => 1,
        "Psn" => 2,
        "Steam" => 3,
        "Blizzard" => 4
    );

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    private function getCurl($method)
    {
        $ch = curl_init($this->mainUrl . $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-API-Key: $this->apiKey"
        ));
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }

    public function searchPlayer($nickName)
    {
        foreach ($this->arTypes as $currentType) {
            $currentProfile = $this->getCurl("/Destiny2/SearchDestinyPlayer/" . $currentType . "/" .
                rawurlencode($nickName) . "/");
            if (!empty($currentProfile->Response))
                $arProfiles[] = $currentProfile->Response[0];
        }
        return $arProfiles;
    }

    public function getProfile($membershipId, $membershipType)
    {
        return $this->getCurl("/Destiny2/" . $membershipType . "/Profile/" . $membershipId . "/?components=Collectibles");
    }

    public function getTypeName($membershipType) {
        return array_search($membershipType, $this->arTypes);
    }
}